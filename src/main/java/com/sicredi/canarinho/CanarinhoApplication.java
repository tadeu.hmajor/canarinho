package com.sicredi.canarinho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class CanarinhoApplication {

	public static void main(String[] args) {

		SpringApplication.run(CanarinhoApplication.class, args);
		System.out.println(new BCryptPasswordEncoder().encode("senha123"));
	}

}
