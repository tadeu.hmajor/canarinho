package com.sicredi.canarinho.DTO;

import com.sicredi.canarinho.model.Anotacao;
import com.sicredi.canarinho.model.Ave;
import com.sicredi.canarinho.model.Usuario;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@Builder
public class AnotacaoDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    private LocalDateTime data;
    private String localizacao;
    private String titulo;
    private Usuario usuario;
    private Ave ave;

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }


    public AnotacaoDTO(Anotacao ent) {
        //this.id = ent.getId_anotacao();
        this.data = ent.getData();
        this.localizacao = ent.getLocalizacao();
        this.titulo = ent.getTitulo();
        this.usuario = ent.getUsuario();
        this.ave = ent.getAve();
    }



    //    private Anotacao (AnotacaoDTO anotacaoDTO){
//        Anotacao anotacao = modelMapper().map(anotacaoDTO, Anotacao.class);
//        return anotacao;
//    }
}
