package com.sicredi.canarinho.DTO;


import com.sicredi.canarinho.model.Role;
import com.sicredi.canarinho.model.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO {
    private Long id;
    private String nome;
    private String usermane;


    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

    public UsuarioDTO(Usuario ent) {
        this.id = ent.getId();
        this.nome = ent.getNome();
        this.usermane = ent.getUsername();

    }

    private Usuario converterParaEntidade(UsuarioDTO usuarioDTO){
        Usuario usuario = modelMapper().map(usuarioDTO, Usuario.class);
        return usuario;
    }
    public UsuarioDTO converterParaDto(UsuarioDTO usuario){
        UsuarioDTO usuarioDTO = modelMapper().map(usuario, UsuarioDTO.class);
        return usuarioDTO;
    }

}



