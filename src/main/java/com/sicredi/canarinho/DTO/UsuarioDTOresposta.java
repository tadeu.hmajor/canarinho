package com.sicredi.canarinho.DTO;

import com.sicredi.canarinho.model.Role;
import com.sicredi.canarinho.model.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;

import java.util.LinkedList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTOresposta {

    private Long id;
    private String nome;
    private String username;
    private String password;


    public UsuarioDTOresposta(Usuario ent) {
        this.id = ent.getId();
        this.nome = ent.getNome();
        this.username = ent.getUsername();
        this.password = ent.getPassword();

    }


    private  UsuarioDTOresposta converterParaDto(Usuario usuario){
        UsuarioDTOresposta usuarioDTOresposta = modelMapper().map(usuario, UsuarioDTOresposta.class);
        return usuarioDTOresposta;
    }



    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }


}
