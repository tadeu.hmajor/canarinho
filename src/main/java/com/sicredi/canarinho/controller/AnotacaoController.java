package com.sicredi.canarinho.controller;

import com.sicredi.canarinho.DTO.AnotacaoDTO;
import com.sicredi.canarinho.model.Anotacao;
import com.sicredi.canarinho.service.AnotacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(value = "/anotacoes")
public class AnotacaoController {

    @Autowired
    private AnotacaoService service;

    @GetMapping
    public ResponseEntity<Page<AnotacaoDTO>> findAll(Pageable pageable){

        Page<AnotacaoDTO> lista = service.findAllPaged(pageable);
        return ResponseEntity.ok().body(lista);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AnotacaoDTO> findById(@PathVariable Long id){
        AnotacaoDTO dto = service.findById(id);
        return ResponseEntity.ok().body(dto);
    }

    //alterado
    @PostMapping
    public ResponseEntity<AnotacaoDTO> insert(@RequestBody AnotacaoDTO dto){
        dto = service.insert(dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(dto.getId()).toUri();
        return ResponseEntity.created(uri).body(dto);
    }



    @PutMapping(value = "/{id}")
    public ResponseEntity<AnotacaoDTO> update(@PathVariable Long id, @RequestBody AnotacaoDTO dto){
        dto = service.update(id, dto);
        return ResponseEntity.ok().body(dto);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
