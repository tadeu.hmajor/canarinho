package com.sicredi.canarinho.controller;

import com.sicredi.canarinho.model.Ave;
import com.sicredi.canarinho.model.Role;
import com.sicredi.canarinho.model.Usuario;
import com.sicredi.canarinho.service.AveService;
import com.sicredi.canarinho.service.UsuarioService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins ="http://localhost:8080/", maxAge = 3600)
@RequestMapping("/aves")
public class AveController {

    @Autowired
    private AveService aveService;



    @PostMapping
    public ResponseEntity<Ave> addAve(@Valid @RequestBody Ave ave){
        Ave aveAdd = aveService.add(ave);
        return ResponseEntity.status(HttpStatus.CREATED).body(aveAdd);
    }

    @GetMapping
    public ResponseEntity<Page<Ave>>  listaTodasAves(@PageableDefault(page=0, size=10, sort="id", direction = Sort.Direction.ASC) Pageable pageable) {
        return ResponseEntity.status(HttpStatus.OK).body(aveService.listarTodos(pageable));
    }

    @GetMapping("/{id}")
    public  Ave aveId(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(aveService.localizarId(id)).getBody();

    }

    @PutMapping("/{id}")
    public ResponseEntity<Ave> editarAve(@Valid @PathVariable Long id, @RequestBody Ave ave){

        return ResponseEntity.ok(aveService.atualizar(id, ave));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deletarAve(@PathVariable Long id){

        aveService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Ave deletada com sucesso.");
    }


}
