package com.sicredi.canarinho.controller;

import com.sicredi.canarinho.DTO.UsuarioDTO;
import com.sicredi.canarinho.DTO.UsuarioDTOresposta;
import com.sicredi.canarinho.model.Role;
import com.sicredi.canarinho.model.Usuario;
import com.sicredi.canarinho.service.UsuarioService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "usuarios")
public class UsuarioController {

 @Autowired
 private UsuarioService usuarioService;

    @PostMapping(value = "/{login}")
    public ResponseEntity<Boolean> login(@RequestBody UsuarioDTOresposta usuarioDTOresposta){

        return usuarioService.login(usuarioDTOresposta);

    }


    @GetMapping
    public ResponseEntity<List<UsuarioDTO>> findAll() {
        List<UsuarioDTO> list = usuarioService.findAll();

        return ResponseEntity.ok().body(list);

    }

    @GetMapping(value = "/{id}")
    public ResponseEntity <UsuarioDTO> findById(@PathVariable Long id) {
        UsuarioDTO dto = usuarioService.findbyId(id);
        return ResponseEntity.ok().body((dto.converterParaDto(dto)));
    }

    @PostMapping
    public ResponseEntity<UsuarioDTOresposta> insert(@RequestBody UsuarioDTOresposta dto){
        dto = usuarioService.insert(dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dto.getId()).toUri();

        return ResponseEntity.created(uri).body(dto);

    }

    @PutMapping (value = "{id}")
    public ResponseEntity<UsuarioDTOresposta> update(@PathVariable Long id, @RequestBody UsuarioDTOresposta usuarioDTO){
        usuarioDTO = usuarioService.update(id, usuarioDTO);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(usuarioDTO.getId()).toUri();
        return ResponseEntity.created(uri).body(usuarioDTO);

    }

    @DeleteMapping (value = "/{id}")
    public ResponseEntity<UsuarioDTO> delete(@PathVariable Long id) {
        usuarioService.delete(id);
        return ResponseEntity.noContent().build();

    }

    @PutMapping("/{idUsuario}/permissao/{idPerfil}")
    public ResponseEntity<Usuario> addPerfil(@PathVariable Long idUsuario, @PathVariable Long idPerfil){
        return ResponseEntity.ok(usuarioService.addRole(idUsuario,idPerfil));
    }

}