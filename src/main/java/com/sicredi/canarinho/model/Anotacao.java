package com.sicredi.canarinho.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_anotacao")
@Entity
public class Anotacao {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
    //@Column(name = "id")
    private Long id_anotacao;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss a")
    @Column(columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    private LocalDateTime data;
    private String localizacao;


    private String titulo;

    @ManyToOne
    @JoinColumn(name = "tb_usuario", referencedColumnName = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "tb_ave", referencedColumnName = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Ave ave;
}
