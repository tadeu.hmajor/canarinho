package com.sicredi.canarinho.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_ave")
@Entity
public class Ave {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nomPort;
    private String nomIngl;
    private String nomLat;
    private char tam;
    private String cor;
    private String genero;
    private String familia;
    private String habitat;

    @JsonIgnore
    @OneToMany(mappedBy = "ave",  cascade = CascadeType.ALL)
    private List<Anotacao> anotacao;


}