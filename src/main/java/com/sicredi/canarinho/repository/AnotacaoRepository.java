package com.sicredi.canarinho.repository;

import com.sicredi.canarinho.model.Anotacao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnotacaoRepository extends JpaRepository<Anotacao, Long> {
}
