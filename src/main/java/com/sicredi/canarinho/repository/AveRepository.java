package com.sicredi.canarinho.repository;

import com.sicredi.canarinho.model.Ave;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AveRepository extends JpaRepository<Ave, Long> {
}
