package com.sicredi.canarinho.repository;

import com.sicredi.canarinho.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleRepository extends JpaRepository<Role, Long> {
}
