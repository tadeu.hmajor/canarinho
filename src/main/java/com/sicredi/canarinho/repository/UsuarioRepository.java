package com.sicredi.canarinho.repository;

import com.sicredi.canarinho.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;

import java.util.Optional;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findByUsername(String username);
}
