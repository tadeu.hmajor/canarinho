package com.sicredi.canarinho.security;

import com.sicredi.canarinho.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter  {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    public void configure(HttpSecurity http) throws Exception{

        http
                .httpBasic()
                .and()
                .authorizeHttpRequests()

                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/anotacoes/**").hasRole("ADMIN")
                .antMatchers("/criaAveK/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"/usuarios").permitAll() //sem autenticacao
                .antMatchers(HttpMethod.GET,"/usuarios/**").permitAll() //sem autenticacao
                .antMatchers(HttpMethod.PUT,"/usuarios/**").permitAll()
                .antMatchers(HttpMethod.DELETE,"/usuarios/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"/aves").permitAll() //sem autenticacao
                .antMatchers(HttpMethod.GET,"/aves/**").permitAll() //sem autenticacao
                .antMatchers(HttpMethod.PUT,"/aves/**").hasAnyAuthority("ADMIN","USUARIO" )
                .antMatchers(HttpMethod.DELETE,"/aves/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().headers().frameOptions().sameOrigin();

    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        //Como o security busca o usuario por email e analisar a senha criptografada
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    //necessario para validar senha - parte do biblioteca do SpringSecurity

    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception{
        return super.authenticationManager();
    }
}
