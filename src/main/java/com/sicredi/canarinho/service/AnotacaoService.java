package com.sicredi.canarinho.service;

import com.sicredi.canarinho.DTO.AnotacaoDTO;
import com.sicredi.canarinho.DTO.UsuarioDTO;
import com.sicredi.canarinho.model.Anotacao;
import com.sicredi.canarinho.repository.AnotacaoRepository;
import com.sicredi.canarinho.repository.AveRepository;
import com.sicredi.canarinho.repository.UsuarioRepository;
import com.sicredi.canarinho.service.exceptions.DatabaseException;
import com.sicredi.canarinho.service.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.relation.RelationException;
import java.util.Optional;

@Service
public class AnotacaoService {

    @Autowired
    AnotacaoRepository anotacaoRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    AveRepository aveRepository;

    @Transactional(readOnly = true)
    public Page<AnotacaoDTO> findAllPaged(Pageable pageable){
        Page<Anotacao> lista = anotacaoRepository.findAll(pageable);
        return lista.map(x -> new AnotacaoDTO(x));
    }

    @Transactional(readOnly = true)
    public AnotacaoDTO findById(Long id){
        Optional<Anotacao> obj = anotacaoRepository.findById(id);
        Anotacao entidade = obj.orElseThrow(() -> new EntityNotFoundException("Anotação não encontrada"));
        return new AnotacaoDTO(entidade);
    }


    @Transactional
    public AnotacaoDTO insert(AnotacaoDTO dto){
        Anotacao entidade = new Anotacao();
        copyDtoToEntity(dto, entidade);
        entidade = anotacaoRepository.save(entidade);
        return new AnotacaoDTO(entidade);
    }

    //@Transactional
    public void insertKafka(AnotacaoDTO dto) throws RelationException {
        long cont =1;
        Anotacao entidade = new Anotacao();
        entidade.setId_anotacao(cont);
        entidade.setData(dto.getData());
        entidade.setLocalizacao(dto.getLocalizacao());
        entidade.setTitulo(dto.getTitulo());

        entidade.setUsuario(usuarioRepository.findById(dto.getId()).orElseThrow(() -> new RelationException()));
        entidade.setAve(aveRepository.findById(dto.getId()).orElseThrow(() -> new RelationException()));

        anotacaoRepository.save(entidade);
        cont++;

    }

    @Transactional
    public AnotacaoDTO update(Long id, AnotacaoDTO dto){
        try{
            Anotacao entidade = anotacaoRepository.getReferenceById(id);
            copyDtoToEntity(dto, entidade);
            entidade = anotacaoRepository.save(entidade);
            return new AnotacaoDTO(entidade);

        }catch(EntityNotFoundException e){
            throw new EntityNotFoundException("Id " + id + " não encontrado");
        }
    }

    public void delete(Long id){
        try{
            anotacaoRepository.deleteById(id);
        }
        catch(EmptyResultDataAccessException e){
            throw new EntityNotFoundException("Id " + id + " não encontrado");
        }
        catch(DataIntegrityViolationException e){
            throw new DatabaseException("Violação de integridade");
        }
    }

    private void copyDtoToEntity(AnotacaoDTO dto, Anotacao ent){
        ent.setData(dto.getData());
        ent.setLocalizacao(dto.getLocalizacao());
        ent.setTitulo(dto.getTitulo());
        ent.setUsuario(dto.getUsuario());
        ent.setAve(dto.getAve());
    }

}
