package com.sicredi.canarinho.service;

import com.sicredi.canarinho.model.Ave;
import com.sicredi.canarinho.repository.AveRepository;
import com.sicredi.canarinho.service.exceptions.DatabaseException;
import com.sicredi.canarinho.service.exceptions.EntityNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class AveService {

    @Autowired
    private AveRepository aveRepository;
@Transactional
    public Ave add(Ave ave){
        return  aveRepository.save(ave);
    }
@Transactional
    public Page<Ave> listarTodos(Pageable pageable){
        return aveRepository.findAll(pageable);
    }


    @Transactional
    public Ave atualizar(Long id, Ave ave ){
        try {
            Ave atualizaAve = new Ave();
            BeanUtils.copyProperties(ave, atualizaAve);
            atualizaAve.setId(ave.getId()); //manter o mesmo ID
            return aveRepository.save(atualizaAve);

        }
        catch (EntityNotFoundException e){
            throw new EntityNotFoundException("Id não encontrado" + id);
        }
    }

    public  Ave localizarId(Long id){
        Optional<Ave> aveBusca = aveRepository.findById(id);
        if(aveBusca.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return  aveBusca.get();

    }

    @Transactional
    public  void  delete(Long id)throws DatabaseException {
        try {
            aveRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("Não encontrado " + id);

        } catch (DataIntegrityViolationException e) {
            throw new DatabaseException("Integridade violada");
        }
    }

}

