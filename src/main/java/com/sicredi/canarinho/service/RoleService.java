package com.sicredi.canarinho.service;

import com.sicredi.canarinho.model.Role;
import com.sicredi.canarinho.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public Role getRoleById(long id){
        return roleRepository.findById(id).get();
    }


}
