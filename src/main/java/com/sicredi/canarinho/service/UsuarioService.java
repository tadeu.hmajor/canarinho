package com.sicredi.canarinho.service;

import com.sicredi.canarinho.DTO.UsuarioDTO;
import com.sicredi.canarinho.DTO.UsuarioDTOresposta;
import com.sicredi.canarinho.model.Role;
import com.sicredi.canarinho.model.Usuario;
import com.sicredi.canarinho.repository.UsuarioRepository;
import com.sicredi.canarinho.service.exceptions.DatabaseException;
import com.sicredi.canarinho.service.exceptions.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.*;
import java.util.stream.Collectors;

@Service
public class UsuarioService implements UserDetailsService {


    private static Logger logger = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private RoleService roleService;

    @Transactional
    public List<UsuarioDTO> findAll() {
        List<Usuario> list = usuarioRepository.findAll();
        return list.stream().map(x -> new UsuarioDTO(x)).collect(Collectors.toList());

    }

    @Transactional
    public UsuarioDTO findbyId(Long id) {
        Optional<Usuario> obj = usuarioRepository.findById(id);
        Usuario ent = obj.orElseThrow(() -> new EntityNotFoundException("Usuario não encontrado"));
        return new UsuarioDTO(ent);
    }

    @Transactional
    public UsuarioDTOresposta update(Long id, UsuarioDTOresposta dto) {
        try {
            Usuario usuario = usuarioRepository.getReferenceById(id);
            usuario.setNome(dto.getNome());
            usuario.setUsername(dto.getUsername());
            usuario.setPassword(dto.getPassword());
            return new UsuarioDTOresposta(usuario);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Id não encontrado" + id);

        }
    }


    @Transactional
    public UsuarioDTOresposta insert(UsuarioDTOresposta dto) {
        Usuario usuario = new Usuario();
        usuario.setNome(dto.getNome());
        usuario.setUsername(dto.getUsername());
        usuario.setPassword(dto.getPassword());

        usuario.setPassword(passwordEncoder.encode(dto.getPassword()));
        usuario = usuarioRepository.save(usuario);

        return new UsuarioDTOresposta(usuario);
    }


    public void delete(Long id) throws DatabaseException {
        try {
            usuarioRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("Não encontrado" + id);
        }
    }

    //pegar usuario para setar permissao
    public Usuario getUsuarioById(long idUsuario){
        return usuarioRepository.findById(idUsuario).get();
    }

    public Usuario addRole(long idUsuario, long idRole){

       Usuario usuario = getUsuarioById(idUsuario);
       Role role = roleService.getRoleById(idRole);
       Set<Role> roles = new HashSet<>();
       roles.add(role);

       usuario.setRoles(roles);
       return usuarioRepository.save(usuario);

    }

    public ResponseEntity<Boolean> login(UsuarioDTOresposta usuario){

        Usuario usuarioBusca = usuarioRepository.findByUsername(usuario.getUsername());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if(passwordEncoder.matches(usuario.getPassword(), usuarioBusca.getPassword())){
            return  ResponseEntity.status(HttpStatus.OK).body(true);
        }else
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(false);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Usuario usuario = usuarioRepository.findByUsername(username);
        if(usuario ==null) {
            logger.error("usuario não encontrado");
            throw new UsernameNotFoundException("Email não encontrado");
        }
        logger.info("Usuario encontrado " + username);
        return usuario;
    }
}



