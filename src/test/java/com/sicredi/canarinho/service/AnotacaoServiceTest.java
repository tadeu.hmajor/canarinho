package com.sicredi.canarinho.service;

import com.sicredi.canarinho.DTO.AnotacaoDTO;
import com.sicredi.canarinho.model.Anotacao;
import com.sicredi.canarinho.repository.AnotacaoRepository;
import com.sicredi.canarinho.service.exceptions.DatabaseException;
import com.sicredi.canarinho.service.exceptions.EntityNotFoundException;
import com.sicredi.canarinho.testes.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
public class AnotacaoServiceTest {

    @InjectMocks
    private AnotacaoService service;

    @Mock
    private AnotacaoRepository repository;

    private Long idExistente;
    private Long idNaoExistente;
    private Long idDependente;
    private PageImpl<Anotacao> page;
    private Anotacao anotacao;
    private AnotacaoDTO anotacaoDTO;

    @BeforeEach
    void setUp() throws Exception{
        idExistente = 1L;
        idNaoExistente = 2L;
        idDependente = 3L;
        anotacao = Factory.criaAnotacao();
        anotacaoDTO = Factory.criaAnotacaoDto();
        page = new PageImpl<>(List.of(anotacao));

        Mockito.when(repository.save(ArgumentMatchers.any())).thenReturn(anotacao);
        //Mockito.when(repository.save(ArgumentMatchers.)).thenReturn(anotacao);

        Mockito.when(repository.getReferenceById(idExistente)).thenReturn(anotacao);
        Mockito.when(repository.getReferenceById(idNaoExistente)).thenThrow(EntityNotFoundException.class);

        Mockito.when(repository.findById(idExistente)).thenReturn(Optional.of(anotacao));
        Mockito.when(repository.findById(idNaoExistente)).thenThrow(EntityNotFoundException.class);

        Mockito.when(repository.findAll((Pageable) ArgumentMatchers.any())).thenReturn(page);

        Mockito.doNothing().when(repository).deleteById(idExistente);
        Mockito.doThrow(EmptyResultDataAccessException.class).when(repository).deleteById(idNaoExistente);
        Mockito.doThrow(DataIntegrityViolationException.class).when(repository).deleteById(idDependente);
    }

    @Test
    public void insertDeveriaRetornarAnotacaoDtoQuandoAnotacaoCorreta(){

        AnotacaoDTO result = service.insert(anotacaoDTO);

        Assertions.assertEquals(anotacaoDTO, result);

    }

    @Test
    public void updateDeveriaRetornarEntityNotFoundExceptionQuandoIdNaoExistir(){

        Assertions.assertThrows(EntityNotFoundException.class, () -> {

            service.update(idNaoExistente, anotacaoDTO);

        });

        Mockito.verify(repository, Mockito.times(1)).getReferenceById(idNaoExistente);
    }

    @Test
    public void updateDeveriaRetornarAnotacaoDtoQuandoIdExistir(){

        AnotacaoDTO result = service.update(idExistente, anotacaoDTO);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(result, anotacaoDTO);

        Mockito.verify(repository, Mockito.times(1)).getReferenceById(idExistente);
        Mockito.verify(repository, Mockito.times(1)).save(anotacao);
    }

    @Test
    public void findByIdDeveriaRetornarThrowEntityNotFoundExceptionQuandoIdNaoExistir(){

        Assertions.assertThrows(EntityNotFoundException.class, () -> {
           service.findById(idNaoExistente);
        });

        Mockito.verify(repository, Mockito.times(1)).findById(idNaoExistente);
    }

    @Test
    public void findByIdDeveriaRetornarAnotacaoDtoQuandoIdExistir(){

        AnotacaoDTO result = service.findById(idExistente);

        Assertions.assertNotNull(result);

        Mockito.verify(repository, Mockito.times(1)).findById(idExistente);
    }

    @Test
    public void findAllPagedDeveriaRetornarPaginado(){

        Pageable pageable = PageRequest.of(0, 10);
        Page<AnotacaoDTO> result = service.findAllPaged(pageable);

        Assertions.assertNotNull(result);

        Mockito.verify(repository, Mockito.times(1)).findAll(pageable);
    }

    @Test
    public void deleteDeveriaThrowDatabaseExceptionQuandoIdDependent(){
        Assertions.assertThrows(DatabaseException.class, () ->{
            service.delete(idDependente);
        });

        Mockito.verify(repository, Mockito.times(1)).deleteById(idDependente);
    }

    @Test
    public void deleteDeveriaThrowEntityNotFoundExceptionQuandoIdNaoExistir(){

        Assertions.assertThrows(EntityNotFoundException.class, () ->{
            service.delete(idNaoExistente);
        });

        Mockito.verify(repository, Mockito.times(1)).deleteById(idNaoExistente);
    }

    @Test
    public void deleteDeveriaNaoFazerNadaQuandoIdExiste(){

        Assertions.assertDoesNotThrow(() -> {
            service.delete(idExistente);
        });

        Mockito.verify(repository, Mockito.times(1)).deleteById(idExistente);
    }

}

