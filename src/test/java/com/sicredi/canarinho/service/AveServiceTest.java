package com.sicredi.canarinho.service;

import com.sicredi.canarinho.model.Anotacao;
import com.sicredi.canarinho.model.Ave;
import com.sicredi.canarinho.repository.AveRepository;
import com.sicredi.canarinho.service.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class AveServiceTest {

    @InjectMocks
    private AveService aveService;

    @BeforeEach
    void setUp() throws Exception{
        idExistente = 1L;

        Mockito.doNothing().when(aveRepository).deleteById(idExistente);
        Mockito.doThrow(EmptyResultDataAccessException.class).when(aveRepository).deleteById(idNaoExistente);

    };

    @Mock
    private AveRepository aveRepository;
    private long idExistente;
    private long idNaoExistente;

    @Test
    public void testarMetodoDeleteQuandoIdExiste(){

        Assertions.assertDoesNotThrow(() -> {
            aveService.delete(idExistente);
        });

        Mockito.verify(aveRepository, Mockito.times(1)).deleteById(idExistente);
    }

    @Test
    public void testarLancandoDaExceptionResourceNotFoundQuantoOIdNaoExiste(){

        Assertions.assertThrows(EntityNotFoundException.class, ()-> {
            aveService.delete(idNaoExistente);
        });
        Mockito.verify(aveRepository, Mockito.times(1)).deleteById(idNaoExistente);

    }

    @Test
    public void salvarAveSucesso(){

       List<Anotacao> listaCanarinho = new ArrayList();
       Ave aveTest= new Ave(1L,"Canario","Canary","Serinus canaria", 'P',"Amarelo","Serinus","Fringillidae","Areas aridas",listaCanarinho);

       when(aveRepository.save(any())).thenReturn(aveTest);

       Assertions.assertNotNull(aveTest);
       Assertions.assertEquals(Ave.class, aveTest.getClass());
       Assertions.assertEquals(idExistente, aveTest.getId());
       Assertions.assertEquals("Canario", aveTest.getNomPort());

       }

     @Test
    public void atualizarAveSucesso(){

        List<Anotacao> listaCanarinho = new ArrayList();
        Ave aveTest= new Ave(1L,"Canario","Canary","Serinus canaria", 'P',"Amarelo","Serinus","Fringillidae","Areas aridas",listaCanarinho);

        aveTest.setNomPort("Canarinho");
        aveTest.setCor("Verde");
        aveTest.setTam('M');
        aveTest.setHabitat("Serrado");

        aveService.atualizar(1L, aveTest);

        Assertions.assertEquals("Canarinho",aveTest.getNomPort());
        Assertions.assertEquals("Verde",aveTest.getCor());
        Assertions.assertEquals('M',aveTest.getTam());
        Assertions.assertEquals("Serrado",aveTest.getHabitat());
    }

}

