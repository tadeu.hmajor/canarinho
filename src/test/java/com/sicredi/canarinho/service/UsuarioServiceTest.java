package com.sicredi.canarinho.service;

import com.sicredi.canarinho.DTO.UsuarioDTO;
import com.sicredi.canarinho.DTO.UsuarioDTOresposta;
import com.sicredi.canarinho.model.Usuario;
import com.sicredi.canarinho.repository.UsuarioRepository;

import com.sicredi.canarinho.service.exceptions.EntityNotFoundException;
import com.sicredi.canarinho.testes.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
public class UsuarioServiceTest {

    @InjectMocks
    private UsuarioService usuarioService;

    @BeforeEach
    void setUp() throws Exception{
        idExistente = 1L;
        idNãoExistente = 10L;
        usuario = Factory.criaUsuario();
        usuarioDTO = Factory.criaUsuarioDTO();

        Mockito.when(usuarioRepository.save(ArgumentMatchers.any())).thenReturn(usuario);

        Mockito.when(usuarioRepository.getReferenceById(idExistente)).thenReturn(usuario);
        Mockito.when(usuarioRepository.getReferenceById(idNãoExistente)).thenThrow(EntityNotFoundException.class);

        Mockito.when(usuarioRepository.findById(idExistente)).thenReturn(Optional.of(usuario));
        Mockito.when(usuarioRepository.findById(idNãoExistente)).thenThrow(EntityNotFoundException.class);

        Mockito.doNothing().when(usuarioRepository).deleteById(idExistente);
        Mockito.doThrow(EmptyResultDataAccessException.class).when(usuarioRepository).deleteById(idNãoExistente);
    };


    @Mock
    private UsuarioRepository usuarioRepository;

    private long idExistente;
    private long idNãoExistente;
    private Usuario usuario;
    private UsuarioDTO usuarioDTO;
    private  UsuarioDTOresposta usuarioDTOresposta;


    @Test
    public void testarMetodoUpdateQuandoIdNaoExiste(){

        Assertions.assertThrows(EntityNotFoundException.class, () ->{
            usuarioService.update(idNãoExistente, usuarioDTOresposta);
        });

        Mockito.verify(usuarioRepository, Mockito.times(1)).getReferenceById(idNãoExistente);
    }

    @Test
    public void testarMetodoUpdateQuandoIdExiste(){
        UsuarioDTOresposta result = usuarioService.update(idExistente, usuarioDTOresposta);

        Assertions.assertNotNull(result);

        Mockito.verify(usuarioRepository, Mockito.times(1)).getReferenceById(idExistente);
        Mockito.verify(usuarioRepository, Mockito.times(1)).save(usuario);
    }

    @Test
    public void testarMetodoFindByIdQuandoIdNaoExiste(){

        Assertions.assertThrows(EntityNotFoundException.class, () ->{
            usuarioService.findbyId(idNãoExistente);
        });

        Mockito.verify(usuarioRepository, Mockito.times(1)).findById(idNãoExistente);
    }

    @Test
    public void testarMetodoFindByIdQuandoIdExiste(){

        UsuarioDTO result = usuarioService.findbyId(idExistente);

        Assertions.assertEquals(usuarioDTO, result);

        Mockito.verify(usuarioRepository, Mockito.times(1)).findById(idExistente);
    }

    @Test
    public void testarMetodoDeleteQuandoIdExiste(){

        Assertions.assertDoesNotThrow(() -> {
            usuarioService.delete(idExistente);
        });

        Mockito.verify(usuarioRepository, Mockito.times(1)).deleteById(idExistente);
    }

    @Test
    public void testarLancandoDaExceptionResourceNotFoundQuantoOIdNãoExiste(){

        Assertions.assertThrows(EntityNotFoundException.class, ()-> {
            usuarioService.delete(idNãoExistente);
        });
        Mockito.verify(usuarioRepository, Mockito.times(1)).deleteById(idNãoExistente);

    }

}
