package com.sicredi.canarinho.testes;


import com.sicredi.canarinho.DTO.AnotacaoDTO;
import com.sicredi.canarinho.DTO.UsuarioDTO;
import com.sicredi.canarinho.model.Anotacao;
import com.sicredi.canarinho.model.Ave;
import com.sicredi.canarinho.model.Usuario;

import java.time.LocalDateTime;
import java.time.Month;

public class Factory {

    public static Usuario criaUsuario(){

        return new Usuario(1L, "Leonardo Battisti", "leo@sicredi.com.br", "12345678", null);
    }

    public static UsuarioDTO criaUsuarioDTO(){

        return new UsuarioDTO(1L,"Leonardo Battisti", "leo@sicredi.com.br");
    }

    public static Ave criaAve(){

        return new Ave(1L,"João-grande", "Maguari Stork", "Ciconia maguari", 'm', "branco", "femea", "Ciconiidae", "campo seco alto", null);
    }

    public static Anotacao criaAnotacao(){

        return new Anotacao(1L, LocalDateTime.of(2020, Month.DECEMBER, 10, 12, 0), "Medianeira - PR", "Ave avistada", criaUsuario(), criaAve());
    }

    public static AnotacaoDTO criaAnotacaoDto(){

        return new AnotacaoDTO(1L, LocalDateTime.of(2020, Month.DECEMBER, 10, 12, 0), "Medianeira - PR", "Ave avistada", criaUsuario(), criaAve());
    }
}
